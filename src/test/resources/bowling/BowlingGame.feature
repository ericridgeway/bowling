Feature: Bowling Game
  As a video game player
  I want to roll all frames of a bowling game
  So that I can see the final score

  Scenario: All gutter balls
    Given I have a new game
    When I score 0 on 20 rolls
    Then The score should be 0

  Scenario: All 1's
    Given I have a new game
    When I score 1 on 20 rolls
    Then The score should be 20

  Scenario: 1 spare
    Given I have a new game
    When I roll a spare
    And I score a roll of 3
    And I score 0 on 17 rolls
    Then The score should be 16

  Scenario: 1 strike
    Given I have a new game
    When I roll a strike
    And I score a roll of 3
    And I score a roll of 4
    And I score 0 on 16 rolls
    Then The score should be 24

  Scenario: Perfect game
    Given I have a new game
    When I score 10 on 12 rolls
    Then The score should be 300