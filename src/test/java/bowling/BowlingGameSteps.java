package bowling;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.runtime.PendingException;

import static org.fest.assertions.api.Assertions.assertThat;

/**
 * Created with IntelliJ IDEA.
 * User: ericridgeway
 * Date: 1/12/13
 * Time: 8:56 PM
 * To change this template use File | Settings | File Templates.
 */
public class BowlingGameSteps {
    Game game;

    private void rollMany(int scorePerRoll, int numberOfRolls) {
        for (int i = 0; i < numberOfRolls; i++) {
            game.roll(scorePerRoll);
        }
    }

    @Given("^I have a new game$")
    public void I_have_a_new_game() throws Throwable {
        game = new Game();
    }

    @When("^I score (\\d+) on (\\d+) rolls$")
    public void I_score_on_rolls(int scorePerRoll, int numberOfRolls) throws Throwable {
        rollMany(scorePerRoll, numberOfRolls);
    }

    @Then("^The score should be (\\d+)$")
    public void The_score_should_be(int score) throws Throwable {
        assertThat(game.score()).isEqualTo(score);
    }

    @And("^I score a roll of (\\d+)$")
    public void I_score_a_roll_of(int score) throws Throwable {
        game.roll(score);
    }

    @When("^I roll a spare$")
    public void I_roll_a_spare() throws Throwable {
        game.roll(4);
        game.roll(6);
    }

    @When("^I roll a strike$")
    public void I_roll_a_strike() throws Throwable {
        game.roll(10);
    }
}
