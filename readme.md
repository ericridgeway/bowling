Edit, 2013/3/5: Looking back at all this, I started to suspect I was using Cucumber and BDD slightly wrong. I've since made [this  Stack Overflow post](http://stackoverflow.com/questions/5315735/should-i-be-using-rspec-or-cucumber/15230014#15230014).

I still love BDD, I just needed to wrap my head around the TYPES of testing. Currently I am trying to just use RSpec (or it's equivelent in other languages) for the 2 seperate concepts I mention in that post:
 - unit testing
 - integration testing

Shiny!

---

I recently noticed this was "public". It's the first thing I ever stored on bitbucket. I was still figuring out git, and bdd, and cucumber  

This is one of my go-throughs of the "bowling game kata". In this case it's from [here](http://butunclebob.com/ArticleS.UncleBob.TheBowlingGameKata), which is the first link that comes up when you google "bowling kata" ;)  

I've learned some since this. A lot got put in git that shouldn't have, and I'm also pretty sure I laid out the java itself a little funky. I'm actually still a little unsure on the perfect way to do it in java, actually. I've set up cucumber projects in like 3 other languages since this though. It's an adventure every time  

Anyways, I considered deleting this repo or making it private. Instead, I think I'll just add this file for now. Note to self: finish getting testing environment set up in Ruby, learn enough Ruby to do the kata, then post that here instead :)
